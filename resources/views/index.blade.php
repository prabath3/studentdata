<h1>Student List</h1>

<table border="1">
    <tr>
        <td>ID</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Email</td>
        <td>Course</td>
    </tr>
    @foreach($students as $student)
        <tr>
            <td>{{$student['id']}}</td>
            <td>{{$student['first_name']}}</td>
            <td>{{$student['last_name']}}</td>
            <td>{{$student['email']}}</td>
            <td>{{$student['course_id']}}</td>
        </tr>
    @endforeach
</table>
<br>
<button type="button"><a href="addStudent">Add new student</a></button>


