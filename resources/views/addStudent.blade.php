<html lang="eng">

<head>
    <title>Add students</title>
</head>

<body>
<form action="" method="POST">
    @csrf
    <div>
        <label for="first_name"></label>
        <input type="text" name="first_name" id="first_name"><br>
        <label for="text" >First Name</label>
    </div>
    <div>
        <label for="last_name"></label>
        <input type="text" name="last_name" id="last_name"><br>
        <label for="text" >Last Name</label>
    </div>
    <div>
        <input type="text" name="email" id="email"><br>
        <label for="email" >Email</label>
    </div>
    <div>
        <label for="course_id"></label><br>
        <select name="course_id" id="course_id">
            @foreach($courses as $course)
                <option value="{{ $course->id }}">{{ $course->name }}</option>
            @endforeach
        </select>
    </div>

<br>
    <button type="submit">Submit</button>

</form>

</body>
</html>
