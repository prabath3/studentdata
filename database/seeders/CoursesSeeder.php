<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        //
        DB::table('courses')->insert([
            ['name' => 'Software Engineering'],
            ['name' => 'Computer Science'],
            ['name' => 'Business Information System'],
        ]);
    }

}
