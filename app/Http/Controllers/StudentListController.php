<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentListController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $student = Student::all();
        return view('index',['students' =>$student]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses =Course::all();
        return view('addStudent',['courses'=>$courses]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
         //dd($request->input('last_name'));

         $student = new Student();
         $student->first_name = $request->first_name;
         $student->last_name = $request->input('last_name');
         $student->email = $request->email;
         $student->course_id = $request->course_id;
         //dd($student);
         $student->save();
         return redirect('index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
