<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentListController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('studentsList/', function () {
    return view('studentsList');
});*/

Route::get('index',[StudentListController::class,'index']);

Route::get('addStudent',[StudentListController::class,'create']);

Route::POST('addStudent',[StudentListController::class,'store']);



